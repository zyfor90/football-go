package routes

import (
	"football/routes/app"

	"github.com/gin-gonic/gin"
)

func Route(route *gin.Engine) {
	app.Route(route)
}
