package app

import (
	"github.com/gin-gonic/gin"
	ClubController "football/app/Modules/Football"
	TestingController "football/app/Modules/Testing"

)

func Route(route *gin.Engine) {
	router := route.Group("api")
	{
		router.POST("/is-contain-letters", TestingController.IsContainLetters)
		
		football := router.Group("/football") 
		{
			football.GET("/club", ClubController.GetClub)
			football.GET("/leaguestanding", ClubController.GetClubPoint)
			football.GET("/rank", ClubController.GetClubRank)
			football.POST("/recordgame", ClubController.CreateRecord)
		}
	}
}
