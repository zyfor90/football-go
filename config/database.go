package config

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	Model "football/app/Models"
	"os"
)

func LoadDatabase() {
	dsn := "host=" + os.Getenv("DB_HOST") + " user=" + os.Getenv("DB_USERNAME") + " password=" + os.Getenv("DB_PASSWORD") + " dbname=" + os.Getenv("DB_NAME") + " port=" + os.Getenv("DB_PORT") + " sslmode=disable TimeZone=Asia/Jakarta"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		println("- Database Failed to Load")
		println("====================================")
		panic(err)
	} else {
		println("- Database Loaded Successfully")
		println("====================================")
		println("")
	}

	sqlDB, err := db.DB()
	if err != nil {
		panic(err)
	}

	// Set DB pooling
	sqlDB.SetMaxIdleConns(10) // Pengaturan berapa jumlah koneksi minimal dibuat (connection)
	sqlDB.SetMaxOpenConns(100) // Pengaturan berapa jumlah koneksi maksimal yang dibuat (connection)
	sqlDB.SetConnMaxIdleTime(5 * 60) // Pengaturan berapa lama koneksi yang sudah tidak digunakan akan dihapus (seconds * minutes)
	sqlDB.SetConnMaxLifetime(60 * 60) // Pengaturan berapa lama koneksi boleh digunakan (seconds * minutes)

	Model.Transaction = db
}