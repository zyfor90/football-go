package config

import (
	"github.com/joho/godotenv"
)

func LoadEnvironment() {
	err := godotenv.Load()
	if err != nil {
		println("")
		println("====================================")
		println("- Enviroment File Failed to Load")
		panic("Error loading .env file")
	} else {
		println("")
		println("====================================")
		println("- Enviroment File Loaded Successfully")
	}
}