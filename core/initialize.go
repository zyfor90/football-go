package core

import (
	"fmt"
	"os"

	"football/routes"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"football/config"
)

var (
	route *gin.Engine
)

var rootCmd = &cobra.Command{
	Use:   os.Getenv("APP_NAME"),
	Short: os.Getenv("APP_NAME"),
	Long:  os.Getenv("APP_NAME"),
	Run: func(_ *cobra.Command, _ []string) {
		if os.Getenv("APP_ENV") == "production" {
			gin.SetMode(gin.ReleaseMode)
		}

		// * Set Global Middleware Here.
		route := gin.New()
		route.SetTrustedProxies([]string{"127.0.0.1"})
		route.Use(cors.Default())
		route.Use(gin.Logger())
		route.Use(gin.Recovery())
		routes.Route(route)

		if err := route.Run(fmt.Sprintf(":%s", os.Getenv("APP_PORT"))); err != nil {
			panic(err)
		}
	},
}

func init() {
	// * Load the environment to cobra
	cobra.OnInitialize(config.LoadEnvironment, config.LoadDatabase)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}