package club

import (
	"gorm.io/gorm"
  "time"
)

type Club struct {
  gorm.Model
  ID        int64 `gorm:"primary_key;column:id;auto_increment"`
  Name 		  string `gorm:"column:name;type:varchar(100);not null"`
  Points     int `gorm:"column:points;type:integer;not null`
  CreatedAt *time.Time `gorm:"column:created_at;type:datetime;not null"`
  UpdatedAt *time.Time `gorm:"column:updated_at;type:datetime;not null"`
  DeletedAt gorm.DeletedAt `gorm:"delete_option:UNDELETE"`
}

func (m Club) TableName() string {
	return "clubs"
}