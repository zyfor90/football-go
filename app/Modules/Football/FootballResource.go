package football

type ClubResponse struct {
	Id   int    `json:"id"`
    Name string `json:"name"`
}

type ClubPointResponse struct {
	Id   int    `json:"id"`
    Name string `json:"name"`
	Point int `json:"points`
}

type ClubRankResponse struct {
	Id   int    `json:"id"`
    Name string `json:"clubname"`
	Rank int 	`json:"standing"`
}