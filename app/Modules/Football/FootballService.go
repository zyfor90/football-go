package football

import (
	"fmt"
	"strings"
	"github.com/gin-gonic/gin"
	GoValidator "github.com/go-playground/validator/v10"
)

func GetAllDataService() (*[]ClubResponse) {
	// * Get All data method from models 
	data := GetAllDataRepostiory();

	result := []ClubResponse{}
	for _, value := range *data {
		response := ClubResponse{
			Id: int(value.ID),
			Name: value.Name,
		}

		// * insert to method response struct
		result = append(result, response)
	}

	return &result
}

func GetAllDataWithPointService() (*[]ClubPointResponse) {
	// * Get All data method from models 
	data := GetAllDataRepostiory();
	fmt.Println(data)
	result := []ClubPointResponse{}
	for _, value := range *data {
		response := ClubPointResponse{
			Id: int(value.ID),
			Name: value.Name,
			Point: value.Points,
		}

		// * insert to method response struct
		result = append(result, response)
	}

	return &result
}

func GetAllDataWithRankService(c *gin.Context) (*[]ClubRankResponse) {
	// * Get Param
	clubname := c.Query("clubname")

	// * Get All data method from models 
	data := GetAllDataWithParamRepostiory(clubname);

	result := []ClubRankResponse{}
	for index, value := range *data {
		response := ClubRankResponse{
			Id: int(value.ID),
			Name: value.Name,
			Rank: index + 1,
		}

		// * insert to method response struct
		result = append(result, response)
	}

	return &result
}

func CreateRecordDataService(c *gin.Context) (string, int) {
	// * Validate the requests here.
	validate := GoValidator.New()

	// * Define Empty Request Struct
	Request := []CreateRequest{}

	// * Bind the requests here.
	c.ShouldBind(&Request)

	// * Validate the requsts on struct
	for _, req := range Request {
		validation := validate.Struct(req)
		if validation != nil {
			for _, err := range validation.(GoValidator.ValidationErrors) {
				message, errorcode := CreateRequestValidate(err.Field(), err.Tag(), err.Param())
	
				return message, errorcode
			}
		}
	}

	for _, req := range Request {
		HomeClub := GetDataRepostiory(req.Clubhomename)
		AwayClub := GetDataRepostiory(req.Clubawayname)
		
		score := strings.Split(req.Score, " ")

		HomePoint := 0;
		AwayPoint := 0;
        if (score[0] > score[2]) {
            // Home Menang - Away Kalah
            HomePoint = 3;
            AwayPoint = 0;
        } else if (score[0] == score[2]) {
            // Draw
            HomePoint = 1;
            AwayPoint = 1;
        } else if (score[0] < score[2]) {
            // Home Kalah - Away Menang
            HomePoint = 0;
            AwayPoint = 3;
        }

		HomeFixedPoint := HomeClub.Points + HomePoint
		AwayFixedPoint := AwayClub.Points + AwayPoint
		fmt.Println(HomeFixedPoint)
		fmt.Println(AwayFixedPoint)


		// * Send The verified request to repository
		Message, ErrorCode := CreateDataRepository(HomeClub.ID, AwayClub.ID, HomeFixedPoint, AwayFixedPoint)
		if len(Message) > 0 {
			return Message, ErrorCode
		}
	}

	return "Berhasil Membuat Data Baru", 200
}