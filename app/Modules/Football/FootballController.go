package football

import (
	"github.com/gin-gonic/gin"
	Responser "football/app/utils"
)

func GetClub(c *gin.Context) {
	Responser.SuccessResponse(c, "Berhasil Menampilkan Data", GetAllDataService(), 200);
}

func GetClubPoint(c *gin.Context) {
	Responser.SuccessResponse(c, "Berhasil Menampilkan Data Point", GetAllDataWithPointService(), 200);
}

func GetClubRank(c *gin.Context) {
	Responser.SuccessResponse(c, "Berhasil Menampilkan Data Point", GetAllDataWithRankService(c), 200);
}

func CreateRecord(c *gin.Context) {
	ResponseMessage, ErrorCode := CreateRecordDataService(c)
	if ErrorCode == 200 {
		Responser.SuccessResponse(c, ResponseMessage, [0]string{}, ErrorCode);
	} else {
		Responser.ErrorResponse(c, ResponseMessage, ErrorCode)
	}
}