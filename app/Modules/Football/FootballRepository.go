package football

import (
	"fmt"
	// "time"
	Model "football/app/Models"
	ClubModel "football/app/Models/Football"
)
func GetDataRepostiory(param string) (*ClubModel.Club) {
	structMethod := ClubModel.Club{}
	err := Model.Transaction.Where("name = ?", param).First(&structMethod).Error
	if err != nil {
		fmt.Println(err)
		return &ClubModel.Club{}
	}
	return &structMethod;
}

func GetAllDataRepostiory() (*[]ClubModel.Club) {
	structMethod := []ClubModel.Club{}
	err := Model.Transaction.Find(&structMethod).Error
	if err != nil {
		fmt.Println(err)
		return &[]ClubModel.Club{}
	}
	return &structMethod;
}

func GetAllDataWithParamRepostiory(param string) (*[]ClubModel.Club) {
	structMethod := []ClubModel.Club{}
	err := Model.Transaction.Where("name ILIKE ?", "%"+param+"%").Find(&structMethod).Error
	if err != nil {
		fmt.Println(err)
		return &[]ClubModel.Club{}
	}
	return &structMethod;
}

func CreateDataRepository(homeID int64, awayID int64, homePoint int, awayPoint int) (string, int) {
	homeStruct := ClubModel.Club{}
	homeErr := Model.Transaction.Model(&homeStruct).Where("id = ?", homeID).Update("points", homePoint).Error
	if homeErr != nil {
		fmt.Println(homeErr)
		return "Internal Server Error", 500
	}

	awayStruct := ClubModel.Club{}
	awayErr := Model.Transaction.Model(&awayStruct).Where("id = ?", awayID).Update("points", awayPoint).Error
	if awayErr != nil {
		fmt.Println(awayErr)
		return "Internal Server Error", 500
	}

	return "", 200
}