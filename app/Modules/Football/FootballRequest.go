package football

type CreateRequest struct {
	Clubhomename string `form:"clubhomename" validate:"required"`
	Clubawayname string `form:"clubawayname" validate:"required"`
	Score string `form:"score" validate:"required"`
}

func CreateRequestValidate(field string, tag string, param string) (string, int)  {
	if field == "Clubhomename" && tag == "required" {
		return "Silahkan Home Club terlebih dahulu", 422
	} else if field == "Clubawayname" && tag == "required" {
		return "Silahkan Away Club terlebih dahulu", 422
	} else if field == "Score" && tag == "required" {
		return "Silahkan Score terlebih dahulu", 422
	} else {
		return "Bad Request", 422
	}
}