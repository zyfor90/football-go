package testing

import (
	"fmt"
	"strings"
	"github.com/gin-gonic/gin"
	GoValidator "github.com/go-playground/validator/v10"
)

func IsContainLettersService(c *gin.Context) (bool, string, int) {
	// * Validate the requests here.
	validate := GoValidator.New()

	// * Define Empty Request Struct
	Request := ValidateRequest{}

	// * Bind the requests here.
	c.ShouldBind(&Request)

	// * Validate the requests
	validation := validate.Struct(&Request)
	if validation != nil {
		for _, err := range validation.(GoValidator.ValidationErrors) {
			fmt.Println(err.Field(), "error:", err.Tag(), "message:", err.Param())
			
			message, errorcode := RequestValidate(err.Field(), err.Tag(), err.Param())

			return false, message, errorcode
		}
	}
	
	search := strings.Index( strings.ToLower(Request.SecondWord) ,  strings.ToLower( Request.FirstWord) )
	if search != -1 {
		return true, "", 200
	}

	return false, "", 200
}
