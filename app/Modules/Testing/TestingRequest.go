package testing

type ValidateRequest struct {
	FirstWord string `form:"first_word" json:"first_word" validate:"required"`
	SecondWord string `form:"second_word" json:"second_word" validate:"required"`
}

func RequestValidate(field string, tag string, param string) (string, int)  {
	if field == "FirstWord" && tag == "required" {
		return "Silahkan first word terlebih dahulu", 422
	} else if field == "SecondWord" && tag == "required" {
		return "Silahkan second word terlebih dahulu", 422
	} else {
		return "Bad Request", 422
	}
}