package testing

import (
	"github.com/gin-gonic/gin"
	Responser "football/app/utils"
)

func IsContainLetters(c *gin.Context) {
	Data, Message, Code := IsContainLettersService(c)
	if Code == 200 {
		Responser.SuccessResponse(c, Message, Data, Code);
	} else {
		Responser.ErrorResponse(c, Message, Code)
	}
}