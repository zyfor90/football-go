package utils

import (
	"fmt"
	"math"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data"`
}

type Meta struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Status  string `json:"status"`
}

type PaginationResponse struct {
	Meta     Meta        `json:"meta"`
	Data     interface{} `json:"data"`
	Paginate Paginate    `json:"pagination"`
}

type Paginate struct {
	Total       int   `json:"total"`
	Count       int   `json:"count"`
	PerPage     int   `json:"per_page"`
	CurrentPage int   `json:"current_page"`
	TotalPages  int   `json:"total_pages"`
	Link        *Link `json:"links"`
}

type Link struct {
	FirstPage interface{} `json:"first_page"`
	LastPage  interface{} `json:"last_page"`
	NextPage  interface{} `json:"next_page"`
	PrevPage  interface{} `json:"prev_page"`
}

func GetUri(c *gin.Context) string {
	scheme := "http://"
	if c.Request.TLS != nil {
		scheme = "https://"
	}

	return scheme + c.Request.Host + c.Request.URL.Path
}


func SuccessResponse(ctx *gin.Context, message string, data interface{}, code int) {
	ctx.JSON(code, Response{
		Meta: Meta{
			Code:    code,
			Message: message,
			Status:  "success",
		},
		Data: data,
	})
	ctx.Abort()
}

func ErrorResponse(ctx *gin.Context, message string, code int) {
	ctx.JSON(code, Response{
		Meta: Meta{
			Code:    code,
			Message: message,
			Status:  "failed",
		},
		Data: nil,
	})
	ctx.Abort()
}

func PaginateResponse(ctx *gin.Context, message string, data interface{}, totalData, dataPerPage, currentPage int, code int) {
	urlPath := GetUri(ctx)
	totalPages := int(math.Ceil(float64(totalData) / float64(dataPerPage)))
	firstPageLink := fmt.Sprintf("%s?page=%d", urlPath, 1)
	lastPageLink := fmt.Sprintf("%s?page=%d", urlPath, totalPages)
	nextPageLink := fmt.Sprintf("%s?page=%d", urlPath, (currentPage + 1))
	prevPageLink := fmt.Sprintf("%s?page=%d", urlPath, (currentPage - 1))

	if (currentPage - 1) == 0 {
		prevPageLink = ""
	}

	if (currentPage + 1) >= totalPages {
		nextPageLink = ""
	}

	dataCount := (currentPage * dataPerPage)
	if totalData < dataPerPage {
		dataCount = totalData
	}

	response := PaginationResponse{
		Meta: Meta{
			Code:    code,
			Message: message,
			Status:  "success",
		},
		Data: data,
		Paginate: Paginate{
			Total:       totalData,
			Count:       dataCount,
			PerPage:     dataPerPage,
			CurrentPage: currentPage,
			TotalPages:  totalPages,
			Link: &Link{
				FirstPage: firstPageLink,
				LastPage:  lastPageLink,
				NextPage:  nextPageLink,
				PrevPage:  prevPageLink,
			},
		},
	}

	ctx.JSON(code, response)
}
