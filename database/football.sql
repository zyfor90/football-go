-- Adminer 4.8.1 PostgreSQL 12.13 dump

DROP TABLE IF EXISTS "clubs";
DROP SEQUENCE IF EXISTS clubs_id_seq;
CREATE SEQUENCE clubs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."clubs" (
    "id" bigint DEFAULT nextval('clubs_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "points" integer DEFAULT '0' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "clubs_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "clubs" ("id", "name", "points", "created_at", "updated_at", "deleted_at") VALUES
(1,	'Man Utd',	0,	'2023-02-14 15:33:52',	'2023-02-14 15:33:52',	NULL),
(2,	'Chelsea',	0,	'2023-02-14 15:33:52',	'2023-02-14 15:33:52',	NULL),
(3,	'Liverpool',	0,	'2023-02-14 15:33:52',	'2023-02-14 15:33:52',	NULL);

DROP TABLE IF EXISTS "match_histories";
DROP SEQUENCE IF EXISTS match_histories_id_seq;
CREATE SEQUENCE match_histories_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."match_histories" (
    "id" bigint DEFAULT nextval('match_histories_id_seq') NOT NULL,
    "club_home" bigint NOT NULL,
    "club_away" bigint NOT NULL,
    "score" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "match_histories_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


ALTER TABLE ONLY "public"."match_histories" ADD CONSTRAINT "match_histories_club_away_foreign" FOREIGN KEY (club_away) REFERENCES clubs(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."match_histories" ADD CONSTRAINT "match_histories_club_home_foreign" FOREIGN KEY (club_home) REFERENCES clubs(id) ON DELETE CASCADE NOT DEFERRABLE;

-- 2023-02-14 22:34:16.69374+07
